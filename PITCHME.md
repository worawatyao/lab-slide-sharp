---
marp: true
title: Marp CLI example
description: Hosting Marp slide deck on the web
# theme: uncover
paginate: true
_paginate: false
---

![bg](./assets/gradient.jpg)

# Sharp

## CRUD สำหรับ admin

<style>a { color: #fff }</style>

https://sharp.code16.fr/docs/

---

# install

```
composer require code16/sharp

php artisan vendor:publish --provider="Code16\Sharp\SharpServiceProvider" --tag=assets

php artisan vendor:publish --provider="Code16\Sharp\SharpServiceProvider" --tag=config
```

---

# Custom URL & Title

file: \config\sharp.php

```php

    "name" => "Lab Admin",

    "custom_url_segment" => "admin",

```

---

![](./screens/sharp.png)

---

# Sample Type

```
php artisan make:model SampleType -m
```

---

## \database\migrations\....\_create_sample_types_table.php

```
        Schema::create('sample_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
```

---

```
$ php artisan migrate
Migrating: 2020_09_09_113111_create_sample_types_table
Migrated:  2020_09_09_113111_create_sample_types_table (12.00ms)
```

---

# building an Entity List

https://sharp.code16.fr/docs/guide/building-entity-list.html

```
php artisan sharp:make:entity-list SampleTypeShartList --model=Models/SampleType
```

จะได้ file SampleTypeSharpList.php ใน \app\Sharp

---

```
class SampleTypeShartList extends SharpEntityList
{
    /**
    * Build list containers using ->addDataContainer()
    *
    * @return void
    */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('name')
                ->setLabel('Name')
                ->setSortable()
                ->setHtml()
        );
    }

    /**
    * Build list layout using ->addColumn()
    *
    * @return void
    */

    public function buildListLayout()
    {
        $this->addColumn('name', 12);
    }

    /**
    * Build list config
    *
    * @return void
    */
    public function buildListConfig()
    {
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('name', 'asc')
            ->setPaginated();
    }

    /**
    * Retrieve all rows data as array.
    *
    * @param EntityListQueryParams $params
    * @return array
    */
    public function getListData(EntityListQueryParams $params)
    {
        return $this->transform(SampleType::all());
    }
}
```

---

# Configure the entity

in \config\sharp.php

```
    "entities" => [
        "SampleType" => [
            "list" => \App\Sharp\SampleTypeShartList::class
        ]
    ]
```

---

# Add Menu

in \config\sharp.php

```
    "menu" => [
        [
            "label" => "Base Table",
            "entities" => [
                [
                    "label" => "Sample Type",
                    "entity" => "SampleType"
                ]
            ]
        ]
    ]
```

---

![](./screens/sharp_sample_type_list.png)

---

# Edit Form

```
php artisan sharp:make:form SampleTypeSharpForm --model=Models/SampleType
```

---

## app\Sharp\SampleTypeSharpForm.php

```
<?php

namespace App\Sharp;

use App\Models\SampleType;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\SharpForm;

class SampleTypeSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;

    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        return $this->transform(
            SampleType::findOrFail($id)
        );
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        $sampleType = $id ? SampleType::findOrFail($id) : new SampleType;
        $this->save($sampleType, $data);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        SampleType::findOrFail($id)->find($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('name')
                ->setLabel('Name')
        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField('name');
        });
    }
}

```

---

## config\sharp.php

```
    "entities" => [
        "SampleType" => [
            "list" => \App\Sharp\SampleTypeShartList::class,
            "form" => \App\Sharp\SampleTypeSharpForm::class,
        ]
    ]
```

---

![](./screens/sharp_sample_type_form.png)

---

https://sharp.code16.fr/docs/guide/building-entity-form.html#buildformfields
![](./screens/sharp_form_fields.png)

---

# Package Type

```

php artisan make:model PackageType -m

use SoftDeletes;

$table->stirng('name');
$table->softDeletes();

php artisan sharp:make:entity-list PackageTypeList --model=Models/PackageType

php artisan sharp:make:form PackageTypeForm --model=Models/PackageType

```

---

# Parameter and ParameterName

![width:700](./screens/parameter_er.png)

---

# Parameter

```
        Schema::create('parameters', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->timestamps();
        });
```

---

# Parameter Name

```
        Schema::create('parameter_names', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('parameter_id')->constrained('parameters');
            $table->timestamps();
        });
```

---

# Parameter Model

```
class Parameter extends Model
{
    use HasFactory;

    public function names() {
        return $this->hasMany('App\Models\ParameterName');
    }
}
```

---

# ParameterName Model

```
class ParameterName extends Model
{
    use HasFactory;

    public function parameter() {
        return $this->belongsTo('App\Models\Parameter', 'parameter_id');
    }
}
```

---

# Factory

```
php artisan make:factory ParameterFactory --model=Parameter

php artisan make:factory ParameterNameFactory --model=ParameterName
```

---

# databases\factories\ParameterFactory

```
class ParameterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Parameter::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => $this->faker->safeColorName()
        ];
    }
}
```

---

# databases\factories\ParameterNameFactory

```
class ParameterNameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ParameterName::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->domainName(),
            'parameter_id' => Parameter::factory(),
        ];
    }
}
```

---

# generate parameter fake data

```
php artisan tinker

Parameter::factory()->has(ParameterName::factory()->count(3), 'names')->count(10)->create()
```

---

# Create Sharp Model List for Parameter

```

php artisan sharp:make:entity-list ParameterList --model=Models/Parameter

```

---

# app\Sharp\ParameterList.php

```
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('code')
                ->setLabel('Code')
                ->setSortable()
                ->setHtml()
        );

        $this->addDataContainer(
            EntityListDataContainer::make('names')
                ->setLabel('Names')
                ->setHtml()
        );
    }
```

---

# app\Sharp\ParameterList.php

```
    public function buildListLayout()
    {
        $this->addColumn('code', 1);
        $this->addColumn('names', 3);
    }
```

---

# app\Sharp\ParameterList.php

```
    public function buildListConfig()
    {
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('code', 'asc')
            ->setPaginated();
    }

```

---

# app\Sharp\ParameterList.php

```
    public function getListData(EntityListQueryParams $params)
    {
        return $this->transform(Parameter::with('names')->get()->map(function($p) {
            return [
                'code' => $p->code,
                'id' => $p->id,
                'names' => $p->namesStr
            ];
        }));
    }

```

---

# app\Models\Parameter.php

```
class Parameter extends Model
{
    use HasFactory;

    public function names() {
        return $this->hasMany('App\Models\ParameterName');
    }

    public function getNamesStrAttribute() {
        return $this->names()->pluck('name')->implode(',');
    }
}
```

---

![](./screens/sharp_parameter_list.png)

---

# Create Paraemter Form

```

php artisan sharp:make:form ParameterForm --model=Models/Parameter

```

---

# ParameterForm

```
    public function find($id): array
    {
        $p = Parameter::findOrFail($id);
        return $this->transform([
            'id' => $p->id,
            'code' => $p->code,
            'names' => $p->names->all()
        ]);
    }

```

---

# ParameterForm

```

    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('code')
                ->setLabel('code')
        )->addField(
            SharpFormListField::make('names')
                ->setLabel('names')
                ->setAddable()
                ->setRemovable()
                ->addItemField(
                    SharpFormTextField::make('name')
                        ->setLabel('name')
                )
        );
    }

```

---

# ParameterForm

```
    public function buildFormLayout()
    {
        $this->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField('code');
        })->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField('names',
                function(FormLayoutColumn $listItem) {
                    $listItem->withSingleField('name');
                });
        });
    }
```

---

# ParameterForm

```
        "Parameter" => [
            "list" => \App\Sharp\ParameterList::class,
            "form" => \App\Sharp\ParameterForm::class,
        ]
```

---

![](./screens/sharp_parameter_form.png)

---

# Unit

```

php artisan make:model Unit -m

# database\migrations\.._create_units_table.php

Schema::create('units', function (Blueprint $table) {
    $table->id();
    $table->string('name');
    $table->timestamps();
});

```

---

# Sharp List & Form

```

php artisan sharp:make:entity-list UnitList --model=Models/Unit

php artisan sharp:make:form UnitForm --model=Models/Unit

```

---

![](./screens/sharp_unit_list.png)

---

# Parameter -> unit

![width:700](./screens/unit.png)

---

# migration

```

php artisan make:migration add_unit_to_parameter

```

---

# Many2One

```

class AddUnitToParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parameters', function (Blueprint $table) {
            $table->foreignId('unit_id')->nullable()->constrained('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters', function (Blueprint $table) {
            $table->dropForeign(['unit_id']);
        });
    }
}


```

---

# Parameter Form

```
    public function find($id): array
    {
        $p = Parameter::findOrFail($id);
        return $this->transform([
            'id' => $p->id,
            'code' => $p->code,
            'unit_id' => $p->unit ? $p->unit->id : null,
            'names' => $p->names->all()
        ]);
    }

```

---

# Parameter Form

```
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('code')
                ->setLabel('code')
        )->addField(
            SharpFormSelectField::make(
                "unit_id",
                Unit::pluck("name", "id")->all())
                ->setLabel("Unit")
                ->setDisplayAsDropdown()
        )->addField(
            SharpFormListField::make('names')
                ->setLabel('names')
                ->setAddable()
                ->setRemovable()
                ->addItemField(
                    SharpFormTextField::make('name')
                        ->setLabel('name')
                )
        );
    }
```

---

# Alternate Unit

![width:700](./screens/parameterAlternateUnit.png)

---

# Make migration

```

php artisan make:model ParameterAlternateUnit -m

```

---

# ParameterAlternateUnit migration

```
    public function up()
    {
        Schema::create('parameter_alternate_units', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parameter_id')->constrained('parameters');
            $table->foreignId('unit_id')->constrained('units');
            $table->decimal('factor', 14, 7);
            $table->timestamps();
        });
    }
```

---

# Parameter Model

```
    public function alternateUnits() {
        return $this->hasMany('App\Models\ParameterAlternateUnit');
    }

```

---

# ParameterAlternateUnit Model

```
class ParameterAlternateUnit extends Model
{
    use HasFactory;

    public function parameter() {
        return $this->belongsTo('App\Models\Parameter');
    }

    public function unit() {
        return $this->belongsTo('App\Models\Unit');
    }
}
```

---

# ParameterForm

```
    public function buildFormFields()
    {
        ...

        )->addField(
            SharpFormListField::make('alternateUnits')
                ->setLabel('Alternate Units')
                ->setAddable()->setAddText('เพิ่มหน่วย')
                ->setRemovable()
                ->addItemField(
                    SharpFormNumberField::make('factor')
                        ->setLabel("factor")
                )->addItemField(
                    SharpFormSelectField::make(
                        "unit_id",
                        Unit::pluck("name", "id")->all())
                        ->setLabel("Unit")
                        ->setDisplayAsDropdown()
                )
        );
    }
```

---

# ParameterForm

```
    public function buildFormLayout()
    {
        $this->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField('code');


        })->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField('names',
                function(FormLayoutColumn $listItem) {
                    $listItem->withSingleField('name');
                });
        });

        $this->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField('unit_id');

        })->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField('alternateUnits',
                function(FormLayoutColumn $item) {
                    $item->withSingleField('factor');
                    $item->withSingleField('unit_id');
                });
        });
    }
```

---

# ParameterForm

```
    public function find($id): array
    {
        $p = Parameter::findOrFail($id);
        return $this->transform([
            'id' => $p->id,
            'code' => $p->code,
            'unit_id' => $p->unit ? $p->unit->id : null,
            'names' => $p->names->all(),
            'alternateUnits' => $p->alternateUnits->all()
        ]);
    }
```

---

![width:700](./screens/sharp_parameter_form_with_unit.png)

---

# Machine

![](./screens/machine.png)

---

# Organization

![](./screens/organization.png)

---
